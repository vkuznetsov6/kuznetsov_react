import React, { Component } from 'react';
import { Button, Modal, Table, FormControl, ControlLabel, FormGroup, Glyphicon } from 'react-bootstrap';
import './App.css';
var _ = require('lodash');


let header = {
    number: '#',
    subject:'Предмет',
    teacher: 'Учитель',
    rating: 'Рейтинг'
}

if(localStorage.length === 0){
    let subArray = [];
    let JSONstr = JSON.stringify(subArray)
    localStorage.setItem('subjectList', JSONstr)
}



class List extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        subjects: _.sortBy(JSON.parse(localStorage.getItem('subjectList')), function(o) { return o.rating; }).reverse(),
        length:  JSON.parse(localStorage.getItem('subjectList')).length,
        addSubject: '',
        addTeacher: '',
        applyBtn: 'Добавить предмет',
        show: false,
        title: '',
        displayAddBtn: '',
        displayAddBtn: '',
        editId: 0,
        warningShow: 'warning hidden',
        warningDuplicateShow: 'warning hidden',
        searchForm: '',
        headerModal: '',
        sortArrow:'arrow-down'
      }
      this.addSubject       = this.addSubject.bind(this);
      this.handleSubject    = this.handleSubject.bind(this);
      this.handleTeacher    = this.handleTeacher.bind(this);
      this.handleShow       = this.handleShow.bind(this);
      this.handleClose      = this.handleClose.bind(this);
      this.handleSearch     = this.handleSearch.bind(this);
      this.editSubject      = this.editSubject.bind(this);
      this.applyChange      = this.applyChange.bind(this);
      this.deleteSubject    = this.deleteSubject.bind(this);
      this.sortByRating     = this.sortByRating.bind(this);
    }
    handleClose() {
         this.setState({ 
            show: false,
            addSubject: '',
            addTeacher: '',
            warningShow: 'hidden'
        });
    }

    handleShow() {
         this.setState({ 
            show: true,
            title: 'Предмет',
            displayAddBtn: 'display',
            displayEditBtn: 'hidden',
            headerModal: 'Добавить предмет'
         });
    }

    createId(max){
        if(this.state.subjects.length < 1) {
            max = 1;
            console.log(1)
        }
        else{
        max = _.maxBy(this.state.subjects, 'id').id + 1;
    }
        return max;
    }
    handleSubject(event){
        this.setState({
            addSubject: event.target.value
        })
    }
    handleTeacher(event){
        this.setState({
            addTeacher: event.target.value
        })
    }
    handleSearch(event){
        this.setState({
            searchForm: event.target.value,
            subjects: array
        })
        let localArray = JSON.parse(localStorage.getItem('subjectList'));
        let array = _.filter(localArray, (o, index, key)=>{
           let reg = new RegExp(event.target.value, 'i');
           let arr = _.some(o, _.method('match', reg));
           if(arr === true) {
            return o;
           }
        })
        this.setState({
            subjects: array
        })
    }
    editSubject(event){
        this.setState({ 
           show: true,
           title: 'Предмет',
           addSubject: event.currentTarget.dataset.subject,
           addTeacher: event.currentTarget.dataset.teacher,
           editId: event.currentTarget.dataset.id,
           displayAddBtn: 'hidden',
           displayEditBtn: 'display',
           headerModal: 'Редактировать предмет'
        });
    }
    applyChange(){
        let array = this.state.subjects;
        if(this.searchDuplicate()) {
             this.setState({
                warningDuplicateShow: 'warning'
            }) 
        }
        else{
        array.forEach((item)=>{
            if(!this.state.addSubject || !this.state.addTeacher){
                this.setState({
                    warningShow: 'warning'
                })
            }
            else{
             if(+this.state.editId === item.id) {
                item.subject = this.state.addSubject;
                item.teacher = this.state.addTeacher;
                this.setState({
                    warningShow: 'hidden',
                    warningDuplicateShow: 'hidden',
                    show: false
                })
            }
            }
        })
        let JSONstr = JSON.stringify(array);
        localStorage.setItem('subjectList', JSONstr);
        } 
    }
    deleteSubject(event){
        let array = this.state.subjects;
        array.forEach((item, index)=>{
            if(+event.currentTarget.dataset.id === item.id){
                array.splice(index, 1);
            }
        })
        this.setState({ 
           subjects: array
        });
        let JSONstr = JSON.stringify(array);
        localStorage.setItem('subjectList', JSONstr); 
    }
    searchDuplicate(){
        let localArray = JSON.parse(localStorage.getItem('subjectList'));
        let teaReg = this.state.addTeacher.toLowerCase();
        let subReg = this.state.addSubject.toLowerCase();
        let arrSub = _.some(localArray, {'subject': subReg});
        let arrTeach = _.some(localArray, {'teacher': teaReg});
        if(arrSub && arrTeach) {
            return true;
        }
    }
    sortByRating(){
        this.setState((prevState)=>({
            subjects: [...prevState.subjects].reverse(),
        }))
        if(this.state.sortArrow === 'arrow-down') {
            this.setState({sortArrow: 'arrow-up'})
        }
        else{
            this.setState({sortArrow: 'arrow-down'})
        }
    }

    addSubject(){
        if(this.searchDuplicate()) {
             this.setState({
                warningDuplicateShow: 'warning'
            }) 
        }
        else if(this.state.addSubject.trim() && this.state.addTeacher.trim()){
            let subject = {
                id: this.createId(),
                subject: this.state.addSubject.toLowerCase(),
                teacher: this.state.addTeacher.toLowerCase(),
                rating: '5'
            }
            this.setState((prevState)=>({
                subjects: [...prevState.subjects, subject],
                length: prevState.length + 1,
                addSubject: '',
                addTeacher: '',
                show: false,
                warningShow: 'hidden',
                warningDuplicateShow: 'hidden'
            }))
            let subArray = JSON.parse(localStorage.getItem('subjectList'));
            subArray.push(subject);
            let JSONstr = JSON.stringify(subArray);
            localStorage.setItem('subjectList', JSONstr)
        }
        else {
             this.setState({
                warningShow: 'warning'
            })
        }
    }
    render() {
        return (
            <div className="subject-list">
            <h2>Список предметов</h2><FormControl className='search-form'  placeholder='Поиск' type="text" value={this.state.searchForm} onChange={this.handleSearch}/>
            <Table className="subject-list__table" striped bordered condensed hover>
                <tbody>
                <tr>
                    <th>{header.number}</th>
                    <th>{header.subject}</th>
                    <th>{header.teacher}</th>
                    <th>
                        {header.rating}
                        <Button className='arrow' onClick={this.sortByRating}><Glyphicon glyph={this.state.sortArrow} /></Button>
                    </th>
                    <th></th>
                </tr>
                    {
                        this.state.subjects.map((item, index)=>
                            <tr className='subject-list__row' key={item.id}>
                                <td>{index+1}</td>
                                <td>{item.subject}</td>
                                <td>{item.teacher}</td>
                                <td>{item.rating}</td>
                                <td>
                                    <Button data-id={item.id} data-subject={item.subject} data-teacher={item.teacher} onClick={this.editSubject}>Редактировать</Button>
                                    <Button data-id={item.id} onClick={this.deleteSubject}>Удалить</Button>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>
            <div>
                <Button bsStyle="primary" bsSize="large" onClick={this.handleShow}>
                  Добавить предмет
                </Button>
            </div>
            <div>
              <Modal show={this.state.show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>{this.state.headerModal}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                <form>
                    <ControlLabel><FormControl  placeholder='Введите название' type="text" value={this.state.addSubject} onChange={this.handleSubject}/><span>{this.state.title}</span></ControlLabel>
                    <ControlLabel><FormControl  placeholder='Введите учителя' type="text" value={this.state.addTeacher} onChange={this.handleTeacher}/><span>Учитель</span></ControlLabel>
                </form>
                </Modal.Body>

                <Modal.Footer>
                    <span className={this.state.warningShow}>Заполните все поля</span>
                    <span className={this.state.warningDuplicateShow}>Такой предмет и преподаватель уже есть</span>
                  <Button className={this.state.displayAddBtn} bsStyle="success" onClick={this.addSubject}>Добавить</Button>
                  <Button className={this.state.displayEditBtn} bsStyle="success" onClick={this.applyChange}>Изменить</Button>
                  <Button bsStyle='danger' onClick={this.handleClose}>Отмена</Button>
                </Modal.Footer>
              </Modal>
            </div>
            </div>
            );
}
}


export {List};
