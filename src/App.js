
import React, {Component} from 'react';
import './App.css';
import {Button, Modal, Table, FormControl, ControlLabel, FormGroup, Glyphicon, ButtonGroup, DropdownButton, MenuItem, Label} from 'react-bootstrap';
import {Router, Link, Route} from 'react-router-dom'
var _ = require('lodash');

if (localStorage.getItem('cityList') === null) {
  let subArray = [];
  // let subArray = [{"id": 1, "city": "ccc", "cityInfo": "info1", "rating": "0"}, {"id": 2,"city": "bbb","cityInfo": "info2","rating": "0"}, {"id": 3, "city": "aaa", "cityInfo": "info3", "rating": "0"}];
  let JSONstr = JSON.stringify(subArray);
  localStorage.setItem('cityList', JSONstr)
}
else if (localStorage.getItem('cityList') === 2){
  let subArray = [];
  // let subArray = [{"id": 1, "city": "ccc", "cityInfo": "info1", "rating": "0"}, {"id": 2,"city": "bbb","cityInfo": "info2","rating": "0"}, {"id": 3, "city": "aaa", "cityInfo": "info3", "rating": "0"}];
  let JSONstr = JSON.stringify(subArray);
  localStorage.setItem('cityList', JSONstr)
}


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      citys: _.sortBy(JSON.parse(localStorage.getItem('cityList')), ['rating']).reverse(),
      show: false,
      addCity: '',
      addCity_Info: '',
      modalHeader: '',
      modalBtn: '',
      modalFunc: '',
      modalId: 0,
      countLimit: false,
      cityLimit: false,
      formatErr: false,
      sortCity: false,
      sortInfo: false,
      sortRaiting: true,
      inputErr1: false,      
      inputErr2: false,
      addReady: false,
      sortCityIco: 'glyphicon glyphicon-arrow-down',
      sortRaitingIco: 'glyphicon glyphicon-arrow-up',
      sortInfoIco: 'glyphicon glyphicon-arrow-down',
      infoClass: '',
      cityClass: '',
      ratClass: '',
    }

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.createId = this.createId.bind(this);
    this.cityAdd = this.cityAdd.bind(this);
    this.cityDelete = this.cityDelete.bind(this);
    this.cityEdit = this.cityEdit.bind(this);
    this.handleCity = this.handleCity.bind(this);
    this.handleInfo = this.handleInfo.bind(this);
    this.modalSave = this.modalSave.bind(this);
    this.searchCity = this.searchCity.bind(this);
    this.sortByCity = this.sortByCity.bind(this);
    this.sortByReting = this.sortByReting.bind(this);
    this.sortByInfo = this.sortByInfo.bind(this);


  }

  handleClose() {
    this.setState({
      show: false,
      addCity: '',
      addCity_Info: '',
      countLimit: false,
      formatErr: false,
      inputErr1: false,
      inputErr2: false,
    });
  }

  handleShow() {
    this.setState({
      show: true,
      modalHeader: 'Add city',
      modalBtn: 'Add',
      modalFunc: this.cityAdd,
      addCity: '',
      addCity_Info: '',
    });
  }

  createId(max) {
    if (this.state.citys.length < 1) {
      max = 1;
    }
    else {
      max = _.maxBy(this.state.citys, 'id').id + 1;
    }
    return max;
  }

  handleCity(event) {
    let val = event.target.value.trim();
    let reg = /^[A-ZА-Я]{1}([a-zа-я]{0,11})?([-a-zа-я]{0,1})?([a-zа-я]{0,2})?([-a-zа-я]{0,1})?([a-zа-я]{0,1})?([-a-zа-я]{0,1})?([A-Za-zа-я]{0,1})?([a-zа-я]{0,1})?([-a-zа-я]{0,1})?([a-zА-Яа-я]{0,1})?([a-zа-я]{0,3})?([-a-zа-я]{0,1})?([A-Za-zа-я]{0,1})?([a-zа-я]{0,1})?([-a-zа-я]{0,1})?([a-zА-Яа-я]{0,1})?([a-zа-я]{0,10})?([а-я]{0,7})?( [A-ZА-Я]{1})?([a-zа-я]{1,14})?$/
    if (reg.test(val)) {
      this.setState({
        addCity: event.target.value,
        formatErr: false,
        addReady: true,
        inputErr: false,
      })
    } else if (event.target.value === '') {
      this.setState({
        formatErr: false,
        addReady: false,
      })
    } else {
      this.setState({
        formatErr: true,
      })
    }

    if (event.target.value !== ''){
      this.setState({
        inputErr1: false,
      })
    }
    this.setState({
      addCity: event.target.value,
    })

    if (event.target.value.length > 25) {
      this.setState({
        addCity: this.state.addCity,
        cityLimit: true,
      })
    } else {
      this.setState({
        cityLimit: false,
        addCity: event.target.value
      })
    }

  }

  handleInfo(event) {
    if (event.target.value.length > 200) {
      this.setState({
        addCity_Info: this.state.addCity_Info,
        countLimit: true,
      })
    } else {
      this.setState({
        countLimit: false,
        addCity_Info: event.target.value
      })
    }

    if (event.target.value.trim() !== '') {
      this.setState({
        inputErr2: false,
      })
    } 
  }

  cityDelete(event) {
    let subArray = JSON.parse(localStorage.getItem('cityList'));
    subArray = _.remove(subArray, (item) => {
      return item.id !== +event.target.id
    });
    let JSONstr = JSON.stringify(subArray);
    localStorage.setItem('cityList', JSONstr);

    let localArr = JSON.parse(localStorage.getItem('sightList'));
    localArr = _.remove(localArr, (item) => {
      return item.id !== +event.target.id
    });
    let JS = JSON.stringify(localArr);
    localStorage.setItem('sightList', JS);
    this.setState({
      citys: JSON.parse(localStorage.getItem('cityList'))
    })

  }

  sortByCity() {
    this.setState({
      infoClass: '',
      cityClass: 'here',
      ratClass: '',
    })
    if (this.state.sortCity === true) {
      this.setState({
        sortCity: false,
        sortCityIco: 'glyphicon glyphicon-arrow-down',
        citys: _.sortBy(this.state.citys, ["city"]),
      })
    } else {
      this.setState({
        sortCity: true,
        sortCityIco: 'glyphicon glyphicon-arrow-up',
        citys: _.sortBy(this.state.citys, ["city"]).reverse(),
      })
    }
  }

  sortByInfo() {
    this.setState({
      infoClass: 'here',
      cityClass: '',
      ratClass: '',
    })
    if (this.state.sortCity === true) {
      this.setState({
        sortCity: false,
        sortInfoIco: 'glyphicon glyphicon-arrow-down',
        citys: _.sortBy(this.state.citys, ["cityInfo"]),
      })
    } else {
      this.setState({
        sortCity: true,
        sortInfoIco: 'glyphicon glyphicon-arrow-up',
        citys: _.sortBy(this.state.citys, ["cityInfo"]).reverse(),
      })
    }
  }

  sortByReting() {
    this.setState({
      infoClass: '',
      cityClass: '',
      ratClass: 'here',
    })
    let localArr = JSON.parse(localStorage.getItem('cityList'));
    if (this.state.sortRaiting === true) {
      this.setState({
        sortRaiting: false,
        sortRaitingIco: 'glyphicon glyphicon-arrow-down',
        citys: _.sortBy(localArr, ["rating"]),
      })
    } else {
      this.setState({
        sortRaiting: true,
        sortRaitingIco: 'glyphicon glyphicon-arrow-up',
        citys: _.sortBy(localArr, ["rating"]).reverse(),
      })
    }
  }

  searchCity(event) {
    if ((event.target.value.length === 1) && (event.target.value === ' ')) {
      event.target.value = '';
      this.setState({
        citys: this.state.citys,
      })
    }


    let localArr = JSON.parse(localStorage.getItem('cityList'));
    localArr.map((i) => {
      i.rating = `${i.rating}`
    })
    localArr = _.filter(localArr, (city) => {
      let exep = new RegExp(event.target.value.trim(), 'iy');
        // let c = city.city.match(exep);
        // return c;
        let arr = _.some(city,_ .method('match', exep));
        if(arr === true) {
         return city;
       }
     })

    this.setState({
      citys: localArr,
    })
  }

  cityEdit(event) {
    _.map(this.state.citys, (item) => {
      if (item.id === +event.target.id) {
        this.setState({
          show: true,
          addReady: true,
          modalHeader: 'Edit',
          addCity: item.city,
          addCity_Info: item.cityInfo,
          modalBtn: 'Save',
          modalFunc: this.modalSave,
          modalId: event.target.id,

        })
      }
    });

  }

  modalSave(event) {
    if ((this.state.addCity.trim() !== '') && (this.state.addCity_Info.trim() !== '') && (this.state.addReady)) {

      let subArray = JSON.parse(localStorage.getItem('cityList'));
      _.map(subArray, (item) => {
        if (item.id === +event.target.id) {
          item.city = this.state.addCity;
          item.cityInfo = this.state.addCity_Info;
        }
      });
      let JSONstr = JSON.stringify(subArray);
      localStorage.setItem('cityList', JSONstr);

      this.setState({
        citys: JSON.parse(localStorage.getItem('cityList')),
        show: false,
      })
    }else {
      this.setState({
        inputErr: true,
      })
    }
  }

  cityAdd() {
    if ((this.state.addCity.trim() !== '') && (this.state.addCity_Info.trim() !== '') && (this.state.addReady)) {
      let obj = {
        id: this.createId(),
        city: this.state.addCity,
        cityInfo: this.state.addCity_Info,
        rating: '0'
      }
      let subArray = JSON.parse(localStorage.getItem('cityList'));
      subArray.push(obj);
      let JSONstr = JSON.stringify(subArray);
      localStorage.setItem('cityList', JSONstr);

      let localArr = JSON.parse(localStorage.getItem('sightList'));
      localArr.push({id: this.createId(), sight: []});
      let JS = JSON.stringify(localArr);
      localStorage.setItem('sightList', JS);

      this.setState({
        addCity: '',
        addCity_Info: '',
        show: false,
        citys: JSON.parse(localStorage.getItem('cityList')),
        inputErr1: false,
      })
    }
    if (this.state.addCity.trim() === ''){
      this.setState({
        inputErr1: true,
      })
    }

    if (this.state.addCity_Info.trim() === '') {
      this.setState({
        inputErr2: true,
      })
    } 

  }

  render() {
    return (

      <div className="cityList">
      <FormControl type="text" value={this.state.search} placeholder="Search city"
      onChange={this.searchCity}/>
      <hr/>
      <Button bsStyle="primary" onClick={this.handleShow}>
      Add sight
      </Button>
      <div  class="btn1" />
      <Table striped bordered condensed hover>
      <thead>
      <tr>
      <td className="cityList_header">City&nbsp;&nbsp; 
      <Button onClick={this.sortByCity} bsSize="xsmall" className={this.state.cityClass}>
      <Glyphicon onClick={this.sortByCity} glyph={this.state.sortCityIco}/>
      </Button>
      </td>
      <td width="70%" className="cityList_header">Information&nbsp;&nbsp;
      <Button onClick={this.sortByInfo} bsSize="xsmall" className={this.state.infoClass}>
      <Glyphicon onClick={this.sortByInfo} glyph={this.state.sortInfoIco}/>
      </Button>
      </td>
      <td className="cityList_header">Rating&nbsp;&nbsp;
      <Button onClick={this.sortByReting} bsSize="xsmall" className={this.state.ratClass}>
      <Glyphicon onClick={this.sortByReting} glyph={this.state.sortRaitingIco}/>
      </Button>
      </td>
      </tr>
      </thead>
      <tbody>
      {
        this.state.citys.map((city, index) => {
          return <tr key={index}>
          <td class="pre"><ButtonGroup>
          <Button id={city.id} onClick={this.cityDelete} bsSize="xsmall"
          bsStyle="danger"><Glyphicon id={city.id} onClick={this.cityDelete}
          glyph="glyphicon glyphicon-remove"/></Button>
          <Button id={city.id} onClick={this.cityEdit} bsSize="xsmall"
          bsStyle="primary"><Glyphicon id={city.id} onClick={this.cityEdit}
          glyph="glyphicon glyphicon-pencil"/></Button>
          </ButtonGroup>&nbsp;&nbsp;
          <Link to={{ pathname: `/about/${city.id}`, state: { id: `${city.id}` }}}>{city.city}</Link></td>
          <td class="pre">{city.cityInfo}</td>
          <td>{city.rating}</td>
          </tr>
        })

      }

      </tbody>
      </Table>
      <Modal show={this.state.show} onHide={this.handleClose}>
      <Modal.Header closeButton>
      <Modal.Title>{this.state.modalHeader}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <form>
      <FormGroup controlId="formBasicText">

      <ControlLabel>City</ControlLabel>
      <FormControl type="text" value={this.state.addCity} placeholder="Введите город с Большой Буквы"
      onChange={this.handleCity}/>

      <ControlLabel>Information</ControlLabel>
      <FormControl type="text" value={this.state.addCity_Info} placeholder="Добавьте описание города"
      onChange={this.handleInfo}/>

      </FormGroup>
      </form>
      </Modal.Body>
      {
        this.state.formatErr?(<div class="err" bsStyle="danger">Название города вводить с большой буквы без цифр</div>):(<Label/>)
      }
      {
        this.state.inputErr1?(<div class="err" bsStyle="danger">Вы не ввели название города</div>):(<Label/>)
      }
      {
        this.state.inputErr2?(<div class="err" bsStyle="danger">Вы не ввели описание города</div>):(<Label/>)
      }
      {
        this.state.countLimit?(<div class="err" bsStyle="danger">Максимум 200 символов</div>):(<Label/>)
      }
      {
        this.state.cityLimit?(<div class="err" bsStyle="danger">Название города не более 25 символов</div>):(<Label/>)
      }
      <Modal.Footer>
      <Button onClick={this.state.modalFunc} id={this.state.modalId}>{this.state.modalBtn}</Button>
      </Modal.Footer>
      </Modal>

      </div>
      );
  }
}




if (localStorage.getItem('sightList') === null) {
  let subArray = [];
      // let subArray = [{id: 2, sight:[{id: 1, name: 'petr1', reite: 3},{id: 2, name: 'pet1r', reite: 3},{id: 3, name: 'petr1', reite: 3},]},{id: 1, sight:[{id: 1, name: 'petr2', reite: 3},{id: 2, name: 'petr2', reite: 3},{id: 3, name: 'petr2', reite: 3},]},{id: 3, sight:[{id: 1, name: 'petr3', reite: 3},{id: 2, name: 'petr3', reite: 3},{id: 3, name: 'petr3', reite: 3},]}]
      let JSONstr = JSON.stringify(subArray);
      localStorage.setItem('sightList', JSONstr)
    }
    else if (localStorage.getItem('sightList') === 2){
      let subArray = [];
  // let subArray = [{id: 2, sight:[{id: 1, name: 'petr1', reite: 3},{id: 2, name: 'pet1r', reite: 3},{id: 3, name: 'petr1', reite: 3},]},{id: 1, sight:[{id: 1, name: 'petr2', reite: 3},{id: 2, name: 'petr2', reite: 3},{id: 3, name: 'petr2', reite: 3},]},{id: 3, sight:[{id: 1, name: 'petr3', reite: 3},{id: 2, name: 'petr3', reite: 3},{id: 3, name: 'petr3', reite: 3},]}]
  let JSONstr = JSON.stringify(subArray);
  localStorage.setItem('sightList', JSONstr)
}

class About extends Component {
  constructor(props) {
    super(props);
    JSON.parse(localStorage.getItem('cityList')).map((city) => {
      if (+this.props.location.state.id === +city.id) {
       this.state = {
        id: this.props.location.state.id,
        city: city.city,
        cityInfo: city.cityInfo,
        sight: JSON.parse(localStorage.getItem('sightList')),
        curSight: '',
        sightRaite: '',
        allRaite: '',
        curSightId: '',
        modalShow: false,
        modalHeader: '',
        modalBtn: '',
        modalFunc: '',
        countLimit: false,
        formatErr: false,
        sortCity: false,
        sortRaiting: true,
        inputErr1: false,
        inputErr2: false,        
        addReady: false,
      }

    }
  })

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.addSight = this.addSight.bind(this);
    this.sightDelete = this.sightDelete.bind(this);
    this.sightEdit = this.sightEdit.bind(this);
    this.sightSave = this.sightSave.bind(this);
    this.handleSight = this.handleSight.bind(this);
    this.reiteSight = this.reiteSight.bind(this);
    this.sightId = this.sightId.bind(this);
    this.countReite = this.countReite.bind(this);
  }

  sightId(max) {
    let localArr = JSON.parse(localStorage.getItem('sightList'));
    localArr.map((i) => {
      if (+this.state.id === +i.id) {
        if (i.sight.length < 1) {
          max = 1
        } else {
          this.state.sight.map((i) => {
            if (+i.id === +this.state.id) {
              max = _.maxBy(i.sight, 'id').id + 1;
            }
          })
        }
      }
    })
    return max;
  }

  handleShow() {
    this.setState({
      modalShow: true,
      modalHeader: 'Add sight',
      modalFunc: this.addSight,
      modalBtn: 'Add',
      modalRaite: 'Select',
      sightRaite: 'Select',
      curSight: '',
      countLimit: false,
    })
  }

  handleClose() {
    this.setState({
      modalShow: false,
      inputErr1: false,
      inputErr2: false,
    })
  }

  handleSight(event) {

    if (event.target.value.trim().length > 20) {
      event.target.value = this.state.curSight;
      this.setState({
        countLimit: true,
      })
    } else {
      this.setState({
        countLimit: false,
        curSight: event.target.value,
      })
    }

    

    if (event.target.value.trim() !== '') {
      this.setState({
        inputErr1: false,
      })
    }
  }

  reiteSight(event) {
    this.setState({
      sightRaite: event,
    })

    if (event.trim() !== 'Select') {
      this.setState({
        inputErr2: false,
      })
    }
  }

  countReite() {
    let localArr = JSON.parse(localStorage.getItem('cityList'));
    localArr.map((city) => {
      if (+this.state.id === +city.id) {
        JSON.parse(localStorage.getItem('sightList')).map((i) => {
          if (+i.id === +this.state.id) {
            city.rating = 0;
            i.sight.map((i) => {
             city.rating = +city.rating + +i.reite;
           })
            city.rating = Math.round((city.rating/i.sight.length) * 100) / 100;
          }
        })
      }
    })
    let JSONstr = JSON.stringify(localArr);
    localStorage.setItem('cityList', JSONstr);
  }

  addSight(event) {
    if ((this.state.curSight.trim() !== '') && (this.state.sightRaite.trim() !== 'Select')) {
      let subArray = JSON.parse(localStorage.getItem('sightList'));
      subArray.map((i) => {
        if (+i.id === +this.state.id) {
          i.sight.push({id:this.sightId(), name:this.state.curSight, reite:this.state.sightRaite});
        }
      })
      let JSONstr = JSON.stringify(subArray);
      localStorage.setItem('sightList', JSONstr);
      this.setState({
        modalShow: false,
        sight: JSON.parse(localStorage.getItem('sightList')),
      })
      this.countReite();
    }

    if (this.state.curSight.trim() === '') {
      this.setState({
        inputErr1: true,
      })
    }

    if (this.state.sightRaite === 'Select') {
      this.setState({
        inputErr2: true,
      })
    }

  }

  sightDelete(event) {
    let subArray = JSON.parse(localStorage.getItem('sightList'));
    subArray.map((i) => {
      if (+i.id === +this.state.id) {
       _.remove(i.sight, (x) => {
        if (+x.id === +event.target.id) {
          return +x.id
        }
      })
     }
   })

    let JSONstr = JSON.stringify(subArray);
    localStorage.setItem('sightList', JSONstr);

    this.setState({
      sight: JSON.parse(localStorage.getItem('sightList')),
    })
    this.countReite();
  }

  sightEdit(event) {
    let subArray = JSON.parse(localStorage.getItem('sightList'));
    subArray.map((i) => {
      if (+i.id === +this.state.id) {
        i.sight.map((x) => {
          if (+x.id === +event.target.id) {
            this.setState({
              modalShow: true,
              modalHeader: 'Edit sight',
              modalBtn: 'Save',
              modalFunc: this.sightSave,
              curSight: x.name,
              sightRaite: x.reite,
              modalRaite: x.reite,
              curSightId: x.id,
            })
          }
        })
      }
    })
  }

  sightSave(event) {
    if ((this.state.curSight.trim() !== '') && (this.state.sightRaite.trim() !== 'Select')) {
      let subArray = JSON.parse(localStorage.getItem('sightList'));
      subArray.map((i) => {
        if (+i.id === +this.state.id) {
          i.sight.map((x) => {                            
            if (+x.id === +event.target.id) {
              x.name = this.state.curSight;
              x.reite = this.state.sightRaite;
            }
          })
        }
      })
      let JSONstr = JSON.stringify(subArray);
      localStorage.setItem('sightList', JSONstr);

      this.setState({
        modalShow: false,
        sight: JSON.parse(localStorage.getItem('sightList')),
      })
      this.countReite();
    } 

    if (this.state.curSight.trim() === '') {
      this.setState({
        inputErr1: true,
      })
    }

    if (this.state.sightRaite === 'Select') {
      this.setState({
        inputErr2: true,
      })
    }
  }

  render() {
    return (
      <div className="cityPage">
      <Link to={`/`} > <Glyphicon glyph="glyphicon glyphicon-hand-left"/> </Link>
      <Button bsStyle="primary" onClick={this.handleShow}>
      Add sight
      </Button>
      <div  class="btn1" />
      <Table striped bordered condensed hover>
      <thead>
      <tr>
      <td className="cityList_header pre">{this.state.city}</td>
      </tr>
      </thead>
      <tbody>
      {
       <tr>
       <td>{this.state.cityInfo}</td>
       </tr>
     }

     </tbody>
     </Table>

     <Table striped bordered condensed hover>
     <thead>
     <tr>
     <td className="cityList_header">Sights</td>
     <td className="cityList_header">Reting</td>
     </tr>
     </thead>
     <tbody>
     {
      this.state.sight.map((sight) => {
        if (+this.state.id === +sight.id) {
         return sight.sight.map((item, index) => {
          return <tr key={index}><td class="pre"><ButtonGroup>
          <Button id={item.id} onClick={this.sightDelete} bsSize="xsmall"
          bsStyle="danger"><Glyphicon id={item.id} onClick={this.sightDelete}
          glyph="glyphicon glyphicon-remove"/></Button>
          <Button id={item.id} onClick={this.sightEdit} bsSize="xsmall"
          bsStyle="primary"><Glyphicon id={item.id} onClick={this.sightEdit}
          glyph="glyphicon glyphicon-pencil"/></Button>
          </ButtonGroup>&nbsp;&nbsp; {item.name} </td>
          <td>{item.reite}</td></tr>
        })     
       }
     })
    }

    </tbody>
    </Table>
    <Modal show={this.state.modalShow} onHide={this.handleClose}>
    <Modal.Header closeButton>
    <Modal.Title>{this.state.modalHeader}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
    <form>
    <FormGroup controlId="formBasicText" /*validationState={this.getValidationState} */>

    <ControlLabel>Sight</ControlLabel>
    <FormControl type="text" value={this.state.curSight} placeholder="Введите название достопримечательности" onChange={this.handleSight}/>

    <ControlLabel className="rait">Rating -&nbsp;&nbsp;
    <DropdownButton id="dropdown-custom" className="drop" value={this.state.sightRaite} onSelect={this.reiteSight} bsSize="small" title={this.state.sightRaite}>
    <MenuItem eventKey="5">5<Glyphicon glyph="star" /></MenuItem>
    <MenuItem eventKey="4">4<Glyphicon glyph="star" /></MenuItem>
    <MenuItem eventKey="3">3<Glyphicon glyph="star" /></MenuItem>
    <MenuItem eventKey="2">2<Glyphicon glyph="star" /></MenuItem>
    <MenuItem eventKey="1">1<Glyphicon glyph="star" /></MenuItem>
    </DropdownButton>
    </ControlLabel>
    </FormGroup>
    </form>
    </Modal.Body>
    {
      this.state.inputErr1?(<div class="err" bsStyle="danger">Вы не ввели название достопримечательности</div>):(<Label/>)
    }
    {
      this.state.inputErr2?(<div class="err" bsStyle="danger">Вы не поставили оценку</div>):(<Label/>)
    }
    {
      this.state.countLimit?(<div class="err" bsStyle="danger">Название не более 20 символов</div>):(<Label/>)
    }
    <Modal.Footer>
    <Button id={this.state.curSightId} onClick={this.state.modalFunc}>{this.state.modalBtn}</Button>
    </Modal.Footer>
    </Modal>

    </div>
    );
  }
}

export {App, About};
