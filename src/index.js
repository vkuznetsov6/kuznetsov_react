import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
    // import {App} from './App.js'
    import {BrowserRouter, Route} from "react-router-dom";
    import {App, About} from "./App";

    class App1 extends React.Component {
        render() {
            return (
                <BrowserRouter>
                <div>
                <Route exact path="/" component={App}/>
                <Route path="/about/:id" component={About}/>
                </div>
                </BrowserRouter>
                );
        }
    }

    ReactDOM.render(<App1/>, document.getElementById('root'));

    registerServiceWorker();
